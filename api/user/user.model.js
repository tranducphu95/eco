const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    firstName: { type: "string", required: true },
    lastName: { type: "string", required: true },
    email: { type: "string", required: true, unique: true },
    phone: { type: "string", required: true },
    password: { type: "string", required: true },
    roles: { type: "string", required: true, default: "member" },
    isConfirmed: { type: "boolean", required: true, default: 0 },
    confirmOTP: { type: "string", required: false },
    otpTries: { type: "number", required: false, default: 0 },
    status: { type: "boolean", required: true, default: 1 },
  },
  { timestamp: true }
);
module.exports = mongoose.model("User", UserSchema);
