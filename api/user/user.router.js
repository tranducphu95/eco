const router = require('express').Router(),
    userController = require('./user.controller');

router.get('/', userController.getAllUser);
router.get('/:id', userController.getUserById);
router.post('/', userController.createUser);
module.exports = router;