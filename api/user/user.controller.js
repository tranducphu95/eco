const User = require("./user.model");

exports.getAllUser = async (req, res, next) => {
  try {
    let users = await User.find();
    return res.json({ users });
  } catch (error) {
    next(error);
  }
};
exports.getUserById = async (req, res, next) => {
  try {
    let user = await User.findById(req.params.id);
    if (user === null) {
      throw new Error({ message: "User not exits!" });
    }
    return res.json({ user });
  } catch (error) {
    next(error);
  }
};
exports.createUser = async (req, res, next) => {
    try {
        let {_id,...userBody} =req.body;
        let user = new User(userBody);
        let userData = await user.save();
        return res.json({ userData});
    } catch (error) {
        next(error);
    }
}
