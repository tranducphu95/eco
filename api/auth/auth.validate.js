const { body } = require("express-validator");
exports.registerRuler = [
  body("*").escaped(),
  body("firstName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("First name must be specified.")
    .isAlphanumeric()
    .withMessage("First name has non-alphanumeric characters"),
  body("lastName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Last name must be specified.")
    .isAlphanumeric()
    .withMessage("Last name has non-alphanumeric characters"),
  body("email")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Email must be specified.")
    .isEmail()
    .withMessage("Email has non-alphanumeric characters"),
  body("password")
    .isLength({ min: 6 })
    .trim()
    .withMessage("password must be at least 6 characters or greater"),
];
