const bcrypt = require("bcrypt"),
  jwt = require("jsonwebtoken"),
  utility = require("../../helpers/utility"),
  userModel = require("../user/user.model");
exports.register = async (req, res, next) => {
  try {
    const { role, ...userData } = req.body;
    const hashPassword = await bcrypt.hash(req.body.password, 10);
    const confirmOTP = utility.randomNumber(4);

    const user = new userModel({
      ...userData,
      password: hashPassword,
      confirmOTP,
    });

    // send confirmation email
    let html = `<p>Please confirm your account</p><p>OTP:<a href='${process.env.BACKEND_URL}/api/auth/verify-otp?email=${req.body.email}&otp=${confirmOTP}'>confirmOTP</a></p>`;
    await mailer.send()
} catch (error) {
    next(error);
  }
};
