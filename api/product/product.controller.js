const Product = require("./product.model"),
  slugify = require("../../helpers/slugify");


exports.getAllProduct = async (req, res, next) => {
  try {
    let products = await Product.find();
    products = products.map(product =>product.toObject());
    console.log("=====>"+ products);
    res.render("product", { products: products });
  } catch (err) {
    next(err);
  }
};
exports.getProductBySlug = async (req,res, next)=>{
  try {
  
    let product = await Product.findOne({ slug: req.params.slug})
    if(product === null)
      throw new Error({message: 'Product not found'})
      return res.json({product});
    
  } catch (error) {
    next(error);
  }
};
exports.getProductOfCategory = async (req, res, next) => {
  try {
    let products = await Product.find({ category: req.params.slug });  
    return res.status(200).json({ products });
  } catch (err) {
    next(err);
  }
};
exports.getPriceOfProduct = async (req,res, next)=>{
  try {
    let priceForProduct = await Product.find(
      { 
        _id:{$in:req.query.items}
      },
      "title price image inventory"
    ).exec();
    return res.json(priceForProduct);
  } catch (error) {
    next(error);
  }
};

exports.createProduct = async (req, res, next) => {
  try {  
    let slug = req.body.slug;
    slug = !slug ? slugify(req.body.title) : slug;
    const product = new Product({ ...req.body, slug });
    const productData = await product.save();
    return res.status(200).json(productData);
  } catch (err) {
    next(err);
  }
};