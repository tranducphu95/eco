const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    title: { type: "string", required: true },
    slug: { type: "string", unique: true },
    description: { type: "string" },
    price: { type: "number", required: true },
    inventory: { type: Number, required: true },
    image: { type: "string" },
  },
  { timestamp: true }
);
module.exports = mongoose.model('Product', ProductSchema);
