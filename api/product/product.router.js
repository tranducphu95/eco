const express = require("express"),
  router = express.Router(),
  productController = require("./product.controller");

router.get("/price", productController.getPriceOfProduct);
router.get("/category/:slug", productController.getProductOfCategory);
router.get("/", productController.getAllProduct);
router.get("/:slug", productController.getProductBySlug);
// failed
router.post("/create", productController.createProduct);

module.exports = router;
