const express = require("express");
const router = express.Router();
const productRouter = require("../api/product/product.router");
const userRouter = require("../api/user/user.router");


/* GET home page. */
router.get("/", function (req, res) {
    res.render("home");
  });
router.use("/product", productRouter);
router.use("/user", userRouter);
  module.exports = router;
  