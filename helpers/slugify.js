function slugify(text) {
    return text
    .toString()
    .toLowerCase()
    .normalize("NFD")
    .trim()
    .replace(/\s+/g,"-") //replace spaces with -
    .replace(/[^\w-]+/g,"")// remove all non-word characters
    .replace(/--+/g,"-") // replace multiple - with single -
}
module.exports = slugify;
