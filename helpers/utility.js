exports.randomNumber = function(length) {
    var text="";
    var position = "123456789";
    for (var i = 0; i < length; i++) {
        var sup = Math.floor(Math.random() * position.length);
        text += i >0 && sup ==i ? "0" : position.charAt(sup);

    }
    return Number(text);
}