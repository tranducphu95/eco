const express = require("express");
const cookieParser = require("cookie-parser");
const handlebars = require("express-handlebars");
const morgan = require("morgan");
const path = require("path");

const app = express();
const port = 3000;
const indexRouter = require("../routers");
const apiRouter = require("../routers/api");

app.use(morgan("combined"));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// template engine
app.set("views", "./views");
app.engine(
  ".html",
  handlebars({
    extname: ".html",
    partialsDir: "views/partials",
  })
);
//static content
app.use(express.static(path.join(__dirname, "../assets")));

app.set("view engine", ".html");

//Router
app.use("/", indexRouter);
// app.use("/api", apiRouter);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
module.exports = app;
